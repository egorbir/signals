import numpy as np
import matplotlib.pyplot as plt
import pandas

from parameters import *

signal_values = pandas.read_csv('../Task1/signalPoints.csv', names=['t', 'u'], header=0).u.tolist()
noise_values = pandas.read_csv('../Task2/noisePoints.csv', names=['t', 'u'], header=0).u.tolist()
mixture_values = pandas.read_csv('../Task3/mixturePoints.csv', names=['t', 'u'], header=0).u.tolist()

signal_fft = np.fft.fft(signal_values)
noise_fft = np.fft.fft(noise_values)
mixture_fft = np.fft.fft(mixture_values[:8192])
mixture_fft_graph = np.fft.fft(mixture_values)

freq = np.fft.fftfreq(8192, 0.00001)
freq_graph = np.fft.fftfreq(10000, 0.00001)

w_plus = 0
w_minus = 0
for i in range(len(freq)):
    if round(freq[i]) == round(FREQ):
        w_plus = i
        print("Номер отсчета +w:", w_plus)
    if round(freq[i]) == -round(FREQ):
        w_minus = i
        print("Номер отсчета -w:", w_minus)

power_signal = np.abs(mixture_fft[w_plus]) ** 2 + np.abs(mixture_fft[w_minus]) ** 2
print("Мощность сигнала:", power_signal)
power_noise = sum([i ** 2 for i in np.abs(mixture_fft)]) - power_signal
print("Мощность шума:", power_noise)
snr = 10 * np.log10(power_signal / power_noise)
print(f"Практическое значение SNR: {snr}")

fig = plt.figure()
plt.plot(freq_graph, np.abs(signal_fft) / 10000)
plt.title('Амплитудный спектр сигнала')
plt.xlabel('Частота, Гц')
plt.ylabel('Напряжение Us, В')
plt.grid()
#fig.savefig('images/signal_spectrum.png')
plt.axis([FREQ-1000, FREQ+1000, -0.02, 0.1])
#fig.savefig('images/signal_spectrum_part.png')
plt.close()
fig = plt.figure()
plt.plot(freq_graph, np.abs(noise_fft) / 10000)
plt.title('Амплитудный спектр шума')
plt.xlabel('Частота, Гц')
plt.ylabel('Напряжение Un, В')
plt.grid()
#fig.savefig('images/noise_spectrum.png')
plt.close()
fig = plt.figure()
plt.plot(freq_graph, np.abs(mixture_fft_graph) / 10000)
plt.title('Амплитудный спектр смеси')
plt.xlabel('Частота, Гц')
plt.ylabel('Напряжение Us, В')
plt.grid()
#fig.savefig('images/mixture_spectrum.png')
plt.axis([13200, 13650, -0.02, 0.5])
#fig.savefig('images/mixture_spectrum_part.png')
plt.close()
