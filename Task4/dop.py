import math

import matplotlib.pyplot as plt
import numpy as np
import pandas


signal_values = pandas.read_csv('../Task1/signalPoints.csv', names=['t', 'u'], header=0).u.tolist()
noise_values = pandas.read_csv('../Task2/noisePoints.csv', names=['t', 'u'], header=0).u.tolist()

freq = np.fft.fftfreq(10000, 0.00001)
signal_fft = np.fft.fft(signal_values)

norma_noise = np.sqrt(sum([i ** 2 for i in noise_values]))
norma_signal = np.sqrt(sum([i ** 2 for i in signal_values]))

energy = norma_signal ** 2
print("Энергия по временной области:", energy)

spectrum_energy = sum([(i/100) ** 2 for i in np.abs(signal_fft)])
print("Энергия по спектру:", spectrum_energy)

skalar = np.dot(signal_values, noise_values)
print("Скалярное произведение сигнала и шума:", skalar)

cos = skalar / (norma_noise * norma_signal)
angle = math.degrees(math.acos(cos))
print("Угол между сигналами:", angle)

fig = plt.figure()
plt.plot(freq, np.abs(signal_fft))
plt.title('Амплитудный спектр сигнала')
plt.xlabel('Частота, Гц')
plt.ylabel('Напряжение Us, В')
plt.grid()
