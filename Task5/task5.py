import math

from scipy.io import wavfile
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt

audio = "sample2.wav"

rate, data = wavfile.read(audio)
duration = data.shape[0] / rate

print("data.shape[0] = {0}".format(data.shape[0]))
print("rate = {0}".format(rate))
print("duration = {0}".format(duration))

freq1 = np.fft.fftfreq(data.shape[0], 1 / rate)
data_signal = np.fft.fft(data)

fig = plt.figure()
plt.plot(freq1, np.abs(data_signal))
plt.xlabel(u'Frequency, Hz')
plt.ylabel(u'Amplitude')
plt.title(u'Amplitude spectrum of the signal')
plt.grid(True)
# plt.axis([-75000, 75000, -0.02, 14000])
plt.savefig('images/1.png')
plt.axis([37000, 40000, -0.02, 7000])
# plt.savefig('images/2.png')
plt.close()

subcarrier_freq = 38500

b, a = signal.butter(10, 20000, btype='lowpass', fs=rate)
additive_component = signal.filtfilt(b, a, data)

b, a = signal.butter(10, 20000, btype='highpass', fs=rate)
diff_component = signal.filtfilt(b, a, data)

amp_spectrum_3 = plt.figure()
plt.plot(freq1, np.abs(np.fft.fft(additive_component)) / 10000)
plt.xlabel(u'Frequency, Hz')
plt.ylabel(u'Amplitude')
plt.title(u'Amplitude spectrum of the additive signal component')
plt.grid(True)
# plt.savefig('images/3.png')
plt.close()

amp_spectrum_4 = plt.figure()
plt.plot(freq1, np.abs(np.fft.fft(diff_component)) / 10000)
plt.xlabel(u'Frequency, Hz')
plt.ylabel(u'Amplitude')
plt.title(u'Amplitude spectrum of the difference signal component')
plt.grid(True)
# plt.savefig('images/4.png')
plt.close()

t = 0
for i in range(0, len(diff_component)):
    diff_component[i] = diff_component[i] * math.cos(
        2 * math.pi * t * subcarrier_freq)
    t = t + 1 / rate

b, a = signal.butter(10, 15000, btype='lowpass', fs=rate)
diff_component = signal.filtfilt(b, a, diff_component)

for i in range(0, len(diff_component)):
    diff_component[i] = 2 * diff_component[i]

first_channel = np.zeros(len(data))
second_channel = np.zeros(len(data))

for i in range(0, len(data)):
    first_channel[i] = (additive_component[i] + diff_component[i]) / 2
    second_channel[i] = (additive_component[i] - diff_component[i]) / 2

# wavfile.write('2_ch1.wav', rate, first_channel)
# wavfile.write('2_ch2.wav', rate, second_channel)
