import math

import numpy as np
import matplotlib.pyplot as plt

from parameters import *

sig = plt.figure()
plt.axis([0, 0.1, 0.29, 0.71])
plt.title("Cosine signal, full")
plt.xlabel("t, s")
plt.ylabel("U, v")
plt.grid()
x = np.arange(0.0, 0.1, 0.000001).tolist()
y = []

for i in range(0, len(x)):
    y += [AMPLITUDE * np.cos(2 * math.pi * x[i] * FREQ + math.pi * INIT_PHASE / 180) + OFFSET]

plt.plot(x, y)
y_dop = AMPLITUDE * np.cos(2 * math.pi * 0.001 * FREQ + math.pi * INIT_PHASE / 180) + OFFSET
print('Значение в 0.001с:', y_dop)
# plt.savefig("images/signal.png")
plt.show()
