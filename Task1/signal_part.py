import math

import numpy as np
import matplotlib.pyplot as plt

from parameters import *

sig = plt.figure()
plt.axis([0, 0.0005, 0.29, 0.71])
plt.title("Cosine signal, zoom")
plt.xlabel("t, s")
plt.ylabel("U, v")
plt.grid()
x_part = np.arange(0.0, 0.0005, 0.000001).tolist()
y_part = []

for i in range(0, len(x_part)):
    y_part += [AMPLITUDE * np.cos(2 * math.pi * x_part[i] * FREQ + math.pi * INIT_PHASE / 180) + OFFSET]

plt.plot(x_part, y_part)
# plt.savefig("images/signal_part.png")
plt.show()
