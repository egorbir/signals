import csv
import math

import matplotlib.pyplot as plt
import numpy as np

from parameters import *

sig = plt.figure()
plt.axis([0, 0.1, 0.29, 0.71])
plt.title("Cosine discrete signal, full")
plt.xlabel("t, s")
plt.ylabel("U, v")
plt.grid()
x_discrete = np.arange(0.0, 0.1, 1/SAMP).tolist()
y_discrete = []
csv_values = [['t', 'u']]
count = 0

for i in range(0, len(x_discrete)):
    y_value = AMPLITUDE * np.cos(2 * math.pi * x_discrete[i] * FREQ + math.pi * INIT_PHASE / 180) + OFFSET
    y_discrete += [y_value]
    csv_values.append([round(x_discrete[i], 5), round(y_value, 8)])
    count += 1

csv_file = open("signalPoints.csv", "w")

with csv_file:
    writer = csv.writer(csv_file)
    writer.writerows(csv_values)

av = sum(y_discrete) / len(y_discrete)
print('Average:', av)
print('Counts:', count)

plt.plot(x_discrete, y_discrete, '.')
# plt.savefig("images/discrete_signal.png")
plt.show()
