import csv

import numpy as np
import math

import matplotlib.pyplot as plt
import pandas

from parameters import *

fig = plt.figure()
plt.title("Mixture of noise and signal")
plt.xlabel("t, s")
plt.ylabel("U, v")
plt.grid()

# read noise from csv & print max/min values
colNames = ["t", "u"]
noiseData = pandas.read_csv('../Task2/noisePoints.csv', names=colNames, header=0)
t = noiseData.t.tolist()
noise_values = noiseData.u.tolist()
print("Max noise value:", max(noise_values))
print("Min noise values:", min(noise_values))

# create signal with amplitude = 1
sig_values = []
for i in range(0, len(t)):
    sig_values += [1 * np.cos(2 * math.pi * t[i] * FREQ + math.pi * INIT_PHASE / 180) + OFFSET]

# get signal average & a_noise
average = sum(sig_values) / len(sig_values)
a_signal = ((sum([(i - average) ** 2 for i in sig_values])) / len(sig_values)) ** (1 / 2)
print("A_signal: ", a_signal)

a_noise = a_signal / (10 ** (SNR / 20))

# get mixture of noise from csv & created signal
result = []
csv_values = [['t', 'u']]

for i in range(len(t)):
    res_value = sig_values[i] + a_noise * noise_values[i] - average
    result += [res_value]
    csv_values.append([t[i], res_value])

# write mixture to csv
'''csv_file = open("mixturePoints.csv", "w")
with csv_file:
    writer = csv.writer(csv_file)
    writer.writerows(csv_values)'''

# get axis and plot
plt.axis([0.0, 0.0005, -2.1, 2.1])

plt.plot(t, result, '.')
print("A noise:", a_noise)
# plt.savefig("images/mixture_part.png")
# plt.show()
