import csv
import random
import matplotlib.pyplot as plt
import numpy as np

from parameters import *

np.random.seed(1002012012)
x = np.arange(0.0, 0.1, 1/SAMP).tolist()
y_discrete = []
csv_values = [['t', 'u']]
count = 0

for i in range(0, len(x)):
    y = np.random.normal()
    y_discrete += [y]
    csv_values.append([round(x[i], 5), round(y, 8)])
    count += 1

csv_file = open("noisePoints.csv", "w")
with csv_file:
    writer = csv.writer(csv_file)
    writer.writerows(csv_values)

av = sum(y_discrete) / len(y_discrete)

dispersion = 0
for i in y_discrete:
    dispersion += pow((i - av), 2)
dispersion /= (len(y_discrete) - 1)

print("Average:", av)
print("Dispersion:", dispersion)
print("Count:", count)

noise_min = min(y_discrete)
noise_max = max(y_discrete)

fig = plt.figure()
plt.axis([0, 0.1, noise_min + 0.1, noise_max + 0.1])
plt.title("Discrete noise, full")
plt.xlabel("t, s")
plt.ylabel("U, V")
plt.grid()

plt.plot(x, y_discrete, '.')
# plt.savefig("images/noise_full.png")
plt.show()
