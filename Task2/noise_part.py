import random
import matplotlib.pyplot as plt
import numpy as np

from parameters import *

x = np.arange(0.0, 0.0005, 1/SAMP).tolist()
y_discrete = []
count = 0

for i in range(0, len(x)):
    y_discrete += [random.normalvariate(0, NOISE_SIGMA)]
    count += 1

noise_min = min(y_discrete)
noise_max = max(y_discrete)

fig = plt.figure()
plt.axis([0, 0.0005, noise_min + 0.1, noise_max + 0.1])
plt.title("Discrete noise, full")
plt.xlabel("t, s")
plt.ylabel("U, V")
plt.grid()

plt.plot(x, y_discrete, '.')
plt.savefig("images/noise_part.png")
# plt.show()
